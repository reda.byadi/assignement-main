package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Operation;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class OperationRepositoryTest {
    @Autowired
    private OperationRepository operationRepository;
    @Autowired
    private  CompteRepository compteRepository;

    @Test
    @Order(5)
    public void findOperation() {
        Operation versement= (Operation) operationRepository.getById(5L);
        Assertions.assertThat(versement.getId()).isNotNull();
    }

    @Test
    @Order(4)
    public void findAllVersement() {
        List<Operation> allVersements = operationRepository.findAllVersement();
        Assertions.assertThat(allVersements).isNotEmpty();
    }
    @Test
    @Order(3)
    public void findAllVirements() {
        List<Operation> allVirements = operationRepository.findAllVirement();
        Assertions.assertThat(allVirements).isNotEmpty();
    }

    @Test
    @Order(2)
    public void saveVersement() {
        Compte compte2 = new Compte();
        compte2.setNrCompte("010000B025001000");
        compte2.setRib("RIB2");
        compte2.setSolde(BigDecimal.valueOf(140000L));
        compte2.setUtilisateur(null);

        compteRepository.save(compte2);

        Versement v1 = new Versement();
        v1.setMontant(BigDecimal.TEN);
        v1.setNom_prenom_emetteur("Reda Byadi");
        v1.setCompteBeneficiaire(compte2);
        v1.setDateExecution(new Date());
        v1.setMotifOperation("Assignment 2021");
        Operation versement= (Operation) operationRepository.save(v1);
        Assertions.assertThat(versement).isNotNull();
    }
    @Test
    @Order(1)
    public void saveVirement() {
        Compte compte1 = new Compte();
        compte1.setNrCompte("010000A000001000");
        compte1.setRib("RIB1");
        compte1.setSolde(BigDecimal.valueOf(200000L));
        compte1.setUtilisateur(null);

        compteRepository.save(compte1);

        Compte compte2 = new Compte();
        compte2.setNrCompte("010000B025001000");
        compte2.setRib("RIB2");
        compte2.setSolde(BigDecimal.valueOf(140000L));
        compte2.setUtilisateur(null);

        compteRepository.save(compte2);

        Virement v = new Virement();
        v.setMontant(BigDecimal.TEN);
        v.setCompteBeneficiaire(compte2);
        v.setCompteEmetteur(compte1);
        v.setDateExecution(new Date());
        v.setMotifOperation("Assignment 2021");
        Operation virement= (Operation) operationRepository.save(v);
        Assertions.assertThat(virement).isNotNull();
    }


    @Test
    @Order(6)
    public void deleteOperation() {
        operationRepository.deleteById(5L);
        Optional<Operation> operation= operationRepository.findById(5L);
        Assertions.assertThat(operation).isEmpty();

    }


}
