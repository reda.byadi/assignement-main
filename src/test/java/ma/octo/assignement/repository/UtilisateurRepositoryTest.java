package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class UtilisateurRepositoryTest {
    @Autowired
    private  UtilisateurRepository utilisateurRepository;

    @Test
    @Order(2)
    public void findUser() {
        Utilisateur utilisateur= utilisateurRepository.getById(1L);
        Assertions.assertThat(utilisateur.getId()).isNotNull();
    }
    @Test
    @Order(3)
    public void findAllUsers() {
        List<Utilisateur> all = utilisateurRepository.findAll();
        Assertions.assertThat(all).isNotEmpty();
    }

    @Test
    @Order(1)
    public void saveUser() {
        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setUsername("user1");
        utilisateur1.setLastname("last1");
        utilisateur1.setFirstname("first1");
        utilisateur1.setGender("Male");
        Utilisateur newUtilisateur=utilisateurRepository.save(utilisateur1);
        Assertions.assertThat(newUtilisateur).isNotNull();
    }

    @Test
    @Order(4)
    public void deleteUser() {
        utilisateurRepository.deleteById(1L);
        Optional<Utilisateur> utilisateur= utilisateurRepository.findById(1L);
        Assertions.assertThat(utilisateur).isEmpty();

    }
}
