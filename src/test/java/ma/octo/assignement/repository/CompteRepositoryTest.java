package ma.octo.assignement.repository;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Operation;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CompteRepositoryTest {
    @Autowired
    private  CompteRepository compteRepository;

    @Test
    @Order(2)
    public void findCompte() {
        Compte compte= compteRepository.getById(3L);
        Assertions.assertThat(compte.getId()).isNotNull();
    }
    @Test
    @Order(3)
    public void findAllComptes() {
        List<Compte> all = compteRepository.findAll();
        Assertions.assertThat(all).isNotEmpty();
    }

    @Test
    @Order(1)
    public void saveCompte() {
        Compte compte1 = new Compte();
        compte1.setNrCompte("010000A000001000");
        compte1.setRib("RIB1");
        compte1.setSolde(BigDecimal.valueOf(200000L));
        compte1.setUtilisateur(null);
        Compte newCompte=compteRepository.save(compte1);
        Assertions.assertThat(newCompte).isNotNull();
    }

    @Test
    @Order(4)
    public void deleteCompte() {
        compteRepository.deleteById(3L);
        Optional<Compte> compte= compteRepository.findById(3L);
        Assertions.assertThat(compte).isEmpty();

    }
}
