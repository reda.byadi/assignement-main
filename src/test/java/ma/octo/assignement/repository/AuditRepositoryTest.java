package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Audit;
import ma.octo.assignement.domain.AuditVersement;
import ma.octo.assignement.domain.AuditVirement;
import ma.octo.assignement.domain.Compte;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class AuditRepositoryTest {
    @Autowired
    private  AuditRepository auditRepository;

    @Test
    @Order(3)
    public void findAudit() {
        Audit audit= auditRepository.getById(7L);
        Assertions.assertThat(audit.getId()).isNotNull();
    }
    @Test
    @Order(4)
    public void findAllAudits() {
        List<Audit> all = auditRepository.findAll();
        Assertions.assertThat(all).isNotEmpty();
    }

    @Test
    @Order(1)
    public void saveAuditVersement() {
        AuditVersement auditVersement = new AuditVersement();
        auditVersement.setMessage("Versementment depuis XXXXXX vers YYYYY d'un montant de ZZZZ");
        AuditVersement newAuditVersement=auditRepository.save(auditVersement);
        Assertions.assertThat(newAuditVersement).isNotNull();
    }
    @Test
    @Order(2)
    public void saveAuditVirement() {
        AuditVirement auditVirement = new AuditVirement();
        auditVirement.setMessage("Versementment depuis XXXXXX vers YYYYY d'un montant de ZZZZ");
        AuditVirement newAuditVirement=auditRepository.save(auditVirement);
        Assertions.assertThat(newAuditVirement).isNotNull();
    }

    @Test
    @Order(5)
    public void deleteAudit() {
        auditRepository.deleteById(7L);
        Optional<Audit> audit= auditRepository.findById(7L);
        Assertions.assertThat(audit).isEmpty();

    }
}
