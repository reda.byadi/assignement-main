package ma.octo.assignement.serviceImpl;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Operation;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.OperationRepository;
import ma.octo.assignement.service.VersementService;
import ma.octo.assignement.service.VirementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;

@Service
@Transactional
public class VirementServiceImpl implements VirementService {
    public static final int MONTANT_MAXIMAL = 10000;

    Logger LOGGER = LoggerFactory.getLogger(VirementServiceImpl.class);

    @Autowired
    private CompteRepository compteRepository;
    @Autowired
    private OperationRepository virementRepository;
    @Autowired
    private AuditServiceImpl auditService;
    @Override
    public List<Operation> loadAll() {
        return virementRepository.findAllVirement();
    }

    @Override
    public void createTransaction(VirementDto virementDto) throws CompteNonExistantException, TransactionException {


        Compte compteEmetteur = compteRepository.findByNrCompte(virementDto.getNrCompteEmetteur());

        Compte compteBeneficiaire = compteRepository
                .findByNrCompte(virementDto.getNrCompteBeneficiaire());
        BigDecimal montantVirement=virementDto.getMontantVirement();

        if (compteEmetteur == null || compteBeneficiaire == null) {
            System.out.println("Compte Non existant");
            throw new CompteNonExistantException("Compte Non existant");
        }

        if (montantVirement==null || montantVirement.doubleValue() == 0) {
            System.out.println("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (montantVirement.intValue() < 10) {
            System.out.println("Montant minimal de virement non atteint");
            throw new TransactionException("Montant minimal de virement non atteint");
        } else if (montantVirement.intValue() > MONTANT_MAXIMAL) {
            System.out.println("Montant maximal de virement dépassé");
            throw new TransactionException("Montant maximal de virement dépassé");
        }

        if (virementDto.getMotif().trim().length()==0) {
            System.out.println("Motif vide");
            throw new TransactionException("Motif vide");
        }

        if (compteEmetteur.getSolde().subtract(montantVirement).doubleValue() < 0) {
            LOGGER.error("Solde insuffisant pour l'utilisateur");
        }


        compteEmetteur.setSolde(compteEmetteur.getSolde().subtract(montantVirement));
        compteRepository.save(compteEmetteur);

        compteBeneficiaire
                .setSolde(new BigDecimal(compteBeneficiaire.getSolde().doubleValue() + montantVirement.doubleValue()));
        compteRepository.save(compteBeneficiaire);

        Virement virement = new Virement();
        virement.setDateExecution(virementDto.getDate());
        virement.setCompteBeneficiaire(compteBeneficiaire);
        virement.setCompteEmetteur(compteEmetteur);
        virement.setMontant(montantVirement);
        virementRepository.save(virement);

        auditService.audit("Virement depuis " + virementDto.getNrCompteEmetteur() + " vers " + virementDto
                .getNrCompteBeneficiaire() + " d'un montant de " + montantVirement
                , EventType.VIREMENT.getType());
    }
    }



