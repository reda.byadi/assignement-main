package ma.octo.assignement.serviceImpl;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Operation;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.OperationRepository;
import ma.octo.assignement.service.VersementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;
@Service
@Transactional
public class VersementServiceImpl implements VersementService {
    public static final int MONTANT_MAXIMAL = 10000;

    Logger LOGGER = LoggerFactory.getLogger(VersementServiceImpl.class);

    @Autowired
    private CompteRepository compteRepository;
    @Autowired
    private OperationRepository versementRepository;
    @Autowired
    private AuditServiceImpl auditService;



    @Override
    public List<Operation> loadAll() {
        return versementRepository.findAllVersement();
    }
    @Override
    public void createTransaction(VersementDto versementDto) throws CompteNonExistantException, TransactionException {
        String fullName = versementDto.getNom_prenom_emetteur();
        Compte compteBeneficiaire = compteRepository
                .findByRib(versementDto.getRib());
        BigDecimal montantVersement=versementDto.getMontantVersement();

        if (compteBeneficiaire == null) {
            System.out.println("Compte Non existant");
            throw new CompteNonExistantException("Compte Non existant");
        }


        if (montantVersement==null || montantVersement.doubleValue() == 0) {
            System.out.println("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (montantVersement.intValue() < 10) {
            System.out.println("Montant minimal de virement non atteint");
            throw new TransactionException("Montant minimal de virement non atteint");
        } else if (montantVersement.intValue() > MONTANT_MAXIMAL) {
            System.out.println("Montant maximal de virement dépassé");
            throw new TransactionException("Montant maximal de virement dépassé");
        }

        if (versementDto.getMotif().trim().length() == 0) {
            System.out.println("Motif vide");
            throw new TransactionException("Motif vide");
        }

        compteBeneficiaire
                .setSolde(new BigDecimal(compteBeneficiaire.getSolde().doubleValue() + montantVersement.doubleValue()));
        compteRepository.save(compteBeneficiaire);

        Versement versement = new Versement();
        versement.setDateExecution(versementDto.getDate());
        versement.setCompteBeneficiaire(compteBeneficiaire);
        versement.setNom_prenom_emetteur(fullName);
        versement.setMontant(montantVersement);
        versementRepository.save(versement);

        auditService.audit("Versementment depuis " + fullName+ " vers " + versementDto
                .getRib() + " d'un montant de " + montantVersement
                , EventType.VERSEMENT.getType());
    }
}
