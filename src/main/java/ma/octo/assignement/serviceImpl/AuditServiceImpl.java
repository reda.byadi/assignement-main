package ma.octo.assignement.serviceImpl;

import ma.octo.assignement.domain.Audit;
import ma.octo.assignement.domain.AuditVersement;
import ma.octo.assignement.domain.AuditVirement;
import ma.octo.assignement.repository.AuditRepository;
import ma.octo.assignement.service.AuditService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class AuditServiceImpl implements AuditService {

    Logger LOGGER = LoggerFactory.getLogger(AuditServiceImpl.class);

    @Autowired
    private AuditRepository auditRepository;

    @Override
        public void audit(String message,String type) {
            LOGGER.info("Audit de l'événement {}",type);
            if(type.equals("Versement")) {
                AuditVersement audit = new AuditVersement();
                audit.setMessage(message);
                auditRepository.save(audit);
            }else{
                AuditVirement audit = new AuditVirement();
                audit.setMessage(message);
                auditRepository.save(audit);
            }


    }


    //add by me



    /*
    public void auditVirement(String message) {

        LOGGER.info("Audit de l'événement {}", EventType.VIREMENT);

        AuditVirement audit = new AuditVirement();
        //audit.setEventType(EventType.VIREMENT);
        audit.setMessage(message);
        auditRepository.save(audit);
    }
     */

/*
    public void auditVersement(String message) {

        LOGGER.info("Audit de l'événement {}", EventType.VERSEMENT);

        AuditVersement audit = new AuditVersement();
        //audit.setEventType(EventType.VERSEMENT);
        audit.setMessage(message);
        auditRepository.save(audit);
    }
 */
}
