package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Operation;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.OperationRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import org.apache.catalina.LifecycleState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public interface VersementService {
    List<Operation> loadAll();
    void createTransaction(VersementDto versementDto) throws CompteNonExistantException, TransactionException;

}
