package ma.octo.assignement.service;

import ma.octo.assignement.domain.AuditVirement;

public interface AuditService {
    public void audit(String message,String type);
}
