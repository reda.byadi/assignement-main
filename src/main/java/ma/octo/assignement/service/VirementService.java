package ma.octo.assignement.service;

import ma.octo.assignement.domain.Operation;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;

import java.util.List;

public interface VirementService {
    List<Operation> loadAll();
    void createTransaction(VirementDto virementDto) throws CompteNonExistantException, TransactionException;

}
