package ma.octo.assignement.dto;

import lombok.Getter;
import lombok.Setter;
import ma.octo.assignement.domain.Compte;

import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.math.BigDecimal;
import java.util.Date;
@Setter
@Getter
public class VersementDto{

    private BigDecimal montantVersement;
    private Date date;
    private String nom_prenom_emetteur;
    private String rib;
    private String motif;


}
