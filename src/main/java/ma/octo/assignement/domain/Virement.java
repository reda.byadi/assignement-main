package ma.octo.assignement.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "VIREMENT")
@DiscriminatorValue("VIREMENT")
@Getter
@Setter
public class Virement extends Operation{

  @ManyToOne
  private Compte compteEmetteur;

}
