package ma.octo.assignement.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "VERSEMENT")
@DiscriminatorValue("VERSEMENT")
@Setter
@Getter
public class Versement extends Operation{
  @Column
  private String nom_prenom_emetteur;

  }

