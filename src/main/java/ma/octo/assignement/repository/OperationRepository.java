package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Operation;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface OperationRepository<T extends Operation>  extends JpaRepository<T, Long> {


    @Query("from Versement")
    List<Versement> findAllVersement();


    @Query("from Virement")
    List<Virement> findAllVirement();
}
