package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Audit;
import ma.octo.assignement.domain.AuditVirement;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuditRepository extends JpaRepository<Audit, Long> {
}
